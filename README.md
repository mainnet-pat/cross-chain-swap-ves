# Cross-chain Atomic Swap With Bitcoin Cash Using One-Time Verifiably Encrypted Signatures

With this contract, it is possible to perform a cross-chain swap of Bitcoin Cash (BCH) with any non-scriptable chain such as Monero (XMR).
Only one chain must have sufficient scripting capabilities, while the other one does not.
All that is needed of the non-scriptable chain is that it uses elliptic curve signatures and that transactions are immutable after some time.
The scheme doesn't have to be used only to swap with XMR, it can be used for any such non-scriptable chain.

Based on:

- [Hoenisch, P. & Pino, L. (2021). Atomic Swaps between Bitcoin and Monero](https://arxiv.org/pdf/2101.12332.pdf)

The above referenced atomic swap scheme relies on partially pre-signed multisig transactions to constrain transaction contents, and such scheme needs SegWit due to 2nd party malleability (one of the parties re-rolling a signature and posting the same transaction but with altered TXID, breaking any pre-signed descendant's signature).

Bitcoin Cash never activated SegWit, however it has upgraded the Script VM with [OP_CHECKDATASIG](https://github.com/bitcoincashorg/bitcoincash.org/blob/master/spec/op_checkdatasig.md) and [transaction introspection opcodes](https://gitlab.com/GeneralProtocols/research/chips/-/blob/master/CHIP-2021-02-Add-Native-Introspection-Opcodes.md).
This means that multisig is not required because we can implement the swap using covenants, and require a signature against a fixed message to reveal the verifiably encrypted signature.
With covenants, there is no problem of parent's TXID changing, because there will be no signature to invalidate, and the interested party can simply update the prevout reference to the alternative TXID, without breaking the covenant.

## Technical Description

On scriptable chain, the swap contract will be a point time locked contract ([PTLC](https://bitcoinops.org/en/topics/ptlc/)), while on the other chain (XMR) it will be just a normal UTXO.

#### Happy Path

Say Bob has BCH and wants XMR, while Alice has XMR and wants BCH.
The "happy path" is then:

1. Alice and Bob generate their one-time keys for the scriptable chain, and then combine them to construct the XMR pubkey, generate and exchange related signatures and proofs.
2. Bob funds the "swaplock" contract on BCH chain.
3. Alice observes the transaction has been mined, verifies the contents, and proceeds to fund the XMR output.
4. Bob reveals his encrypted signature required to forward the BCH to Alice's output.
5. Alice decrypts the signature and publishes the transaction sending the BCH to her output.
6. Bob extracts Alice's BCH secret key from the decrypted signature.
7. Bob uses Alice's BCH secret key to reconstruct the XMR secret, and then takes the XMR.

#### Refund Path

The contract on the scriptable chain is a 2-transaction contract.
Regular swap uses only one transaction, but if 1st timelock expires, the covenant allows forwarding funds to the 2nd contract: the "refund" PTLC.

There, contract structure is exactly the same but the roles are reversed.
In refund contract it is Alice who learns the secret from Bob decrypting her refund signature to take his BCH back, and in doing so he reveals the missing piece for Alice to take back her XMR.

#### Recovery Path

If Bob is missing in action and doesn't claim the refund on time, the 2nd timelock opens a recovery path which allows forwarding the BCH to Alice's output without needing any signature.
Instead of XMR refund, Alice gets BCH as if trade went through while Bob gets nothing because he never learns the secret needed to unlock the XMR.
Neither does Alice, and so XMR will be locked forever.

## License

[MIT No Attribution](https://opensource.org/license/mit-0/).
